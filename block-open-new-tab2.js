for(var els = document.getElementsByTagName('a'), i = els.length; i--;){
    els[i].href = 'javascript:void(0);';
    els[i].onclick = (function(el){
        return function(){
            window.location.href = el.getAttribute('data-href');
        };
    })(els[i]);
}
